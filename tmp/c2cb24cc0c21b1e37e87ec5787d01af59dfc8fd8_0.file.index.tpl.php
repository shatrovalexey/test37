<?php
/* Smarty version 3.1.31, created on 2018-03-31 21:50:56
  from "Z:\home\test37\view\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5abfca80e60eb5_46604592',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c2cb24cc0c21b1e37e87ec5787d01af59dfc8fd8' => 
    array (
      0 => 'Z:\\home\\test37\\view\\index.tpl',
      1 => 1522518629,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5abfca80e60eb5_46604592 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="forms">
	<form id="login-form" action="/login" class="form" method="post" data-show="#profile-form" data-hide="#login-form,#create-form" data-next="#profile-form">
		<h2>авторизация</h2>
		<label>
			<span>логин:</span>
			<input name="login" required title="введите логин">
			<div class="both"></div>
		</label>
		<label>
			<span>пароль:</span>
			<input type="password" name="password" required title="введите пароль">
			<div class="both"></div>
		</label>
		<label class="error-field"></label>
		<label class="submit-field">
			<input type="submit" value="&rarr;">
			<span>войти</span>
			<div class="both"></div>
		</label>
	</form>

	<form id="create-form" action="/create" method="post" enctype="multipart/form-data" data-hide="#login-form,#create-form" data-show="#profile-form" data-next="#profile-form" class="form">
		<input type="hidden" name="photo" required value="">
		<h2>регистрация</h2>
		<label>
			<span>фото:</span>
			<input type="file" name="photo-input" required accept="image/*" title="укажите фото">
			<div class="both"></div>
		</label>
		<label>
			<span>логин:</span>
			<input name="login" required title="введите логин">
			<div class="both"></div>
		</label>
		<label>
			<span>пароль:</span>
			<input type="password" name="password1" required title="укажите пароль">
			<div class="both"></div>
		</label>
		<label>
			<span>подтверждение пароля:</span>
			<input type="password" name="password2" required title="подтвердите пароль">
			<div class="both"></div>
		</label>
		<label class="error-field"></label>
		<label class="submit-field">
			<input type="submit" value="&rarr;">
			<span>зарегистрироваться</span>
			<div class="both"></div>
		</label>
	</form>

	<form id="profile-form" action="/profile" method="post" class="form nod" data-hide="#login-form,#create-form" data-show="#profile-form" data-process="true">
		<input type="hidden" name="session_id" value="" required>
		<input type="hidden" name="user_id" value="" required>
		<h2>профиль пользователя</h2>
		<label class="error-field"></label>
		<table>
			<tbody>
				<tr>
					<td>логин:</td>
					<td data-field="login" data-type="text"></td>
				</tr>
				<tr>
					<td>создан:</td>
					<td data-field="created" data-type="text"></td>
				</tr>
				<tr>
					<td>фото:</td>
					<td><img data-field="photo" data-type="src" class="profile-photo"></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2">
						<label class="submit-field">
							<a href="/" class="profile-logout">выход</a>
						</label>
					</td>
				</tr>
			</tfoot>
		</table>
	</form>

	<div class="waiter nod"></div>
</div><?php }
}
