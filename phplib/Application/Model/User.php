<?php
	/** пакет моделей
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	namespace Application\Model ;

	/** класс модели пользователя
	* @subpackage \Application\Model модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class User extends \Application\Model {
		/**
		* Профиль пользователя
		* @param array $args - аргументы к методу, данные из $_REQUEST
		* @throws \Exception
		* @return array - данные пользователя
		*/
		public function profile( $args ) {
			/**
			* @var \PDOStatement $sth - подготовленный запрос
			* @var array $result - результат выполнения метода
			*/
			$sth = $this->dbh->prepare( '
SELECT
	`u1`.`id` ,
	`u1`.`login` ,
	`u1`.`created` ,
	`u1`.`photo`
FROM
	`session` AS `s1`

	INNER JOIN `user` AS `u1` ON
	( `s1`.`user_id` = `u1`.`id` )
WHERE
	( `s1`.`id` = :session_id ) AND
	( `s1`.`user_id` = :user_id ) AND
	( `s1`.`expired` IS null )
ORDER BY
	`s1`.`created` DESC
LIMIT 1 ;
			' ) ;
			$sth->execute( array(
				'session_id' => $args[ 'session_id' ] ,
				'user_id' => $args[ 'user_id' ]
			) ) ;
			$result = $sth->fetch( \PDO::FETCH_ASSOC ) ;
			$sth->closeCursor( ) ;

			if ( empty( $result ) ) {
				throw new \Exception( 'информация неверна' ) ;
			}

			return $result ;
		}

		/**
		* Авторизация пользователя
		* @param array $args - аргументы из $_REQUEST
		* @throws \Exception
		* @return array идентификатор сессии
		* result - дополнительные данные для обработки запроса
		*/
		public function login( $args ) {
			if ( empty( $args[ 'login' ] ) ) {
				throw new \Exception( 'логин пуст' ) ;
			}
			if ( empty( $args[ 'password' ] ) ) {
				throw new \Exception( 'пароль пуст' ) ;
			}

			/**
			* @var int $user_id - идентификатор пользователя
			* @var char(40) $session_id - идентификатор сессии
			*/
			$user_id = $this->__fetchColumn( '
SELECT
	`u1`.`id` AS `user_id`
FROM
	`user` AS `u1`
WHERE
	( `u1`.`login` = :login ) AND
	( `u1`.`password` = sha1( :password ) ) ;
			' , array(
				'login' => $args[ 'login' ] ,
				'password' => $args[ 'password' ]
			) ) ;

			if ( empty( $user_id ) ) {
				throw new \Exception( 'no such user' ) ;
			}

			$session_id = $this->__fetchColumn( '
SELECT
	`s1`.`id` AS `session_id`
FROM
	`session` AS `s1`
WHERE
	( `s1`.`user_id` = :user_id ) AND
	( `s1`.`expired` IS null )
ORDER BY
	`s1`.`created` DESC
LIMIT 1 ;
			' , array(
				'user_id' => $user_id
			) ) ;

			if ( empty( $session_id ) ) {
				$session_id = sha1( sha1( uniqid( ) ) . sha1( $user_id ) ) ;

				/**
				* @var \PDOStatement $sth - подготовленный запрос
				*/
				$sth = $this->dbh->prepare( '
INSERT INTO
	`session`
SET
	`user_id` := :user_id ,
	`id` := :session_id ;
				' ) ;
				$sth->execute( array(
					'user_id' => $user_id ,
					'session_id' => $session_id
				) ) ;
				$sth->closeCursor( ) ;
			}

			return array(
				'user_id' => $user_id ,
				'session_id' => $session_id
			) ;
		}

		/**
		* Разавторизация пользователя
		* @param $args - аргументы $_REQUEST
		* @return array - результат
		* @throws \Exception
		* result - дополнительные данные для обработки запроса
		*/
		public function unlogin( $args ) {
			/**
			* @var \PDOStatement $sth - подготовленный запрос
			*/
			$sth = $this->dbh->prepare( '
UPDATE
	`session` AS `s1`
SET
	`s1`.`expired` := current_timestamp( )
WHERE
	( `s1`.`id` = :session_id ) ;
			' ) ;
			$sth->execute( array(
				'session_id' => @$args[ 'session_id' ]
			) ) ;
			$sth->closeCursor( ) ;

			return array(
				'result' => true
			) ;
		}

		/**
		* Регистрация пользователя
		* @param $args - аргументы $_REQUEST
		* @throws \Exception
		* @return array информация о пользователе
		* result - дополнительные данные для обработки запроса
		*/
		public function create( $args ) {
			if ( empty( $args[ 'login' ] ) ) {
				throw new \Exception( 'имя пользователя пустое' ) ;
			}

			if ( empty( $args[ 'password1' ] ) ) {
				throw new \Exception( 'пароль пуст' ) ;
			}

			if ( $args[ 'password1' ] !== $args[ 'password2' ] ) {
				throw new \Exception( 'пароль и проверочный пароль не одинаковы' ) ;
			}

			/**
			* @var int $user_id - идентификатор пользователя
			*/
			$user_id = $this->__fetchColumn( '
SELECT
	`u1`.`id` AS `user_id`
FROM
	`user` AS `u1`
WHERE
	( `u1`.`login` = :login )
LIMIT 1 ;
			' , array(
				'login' => @$args[ 'login' ]
			) ) ;

			if ( ! empty( $user_id ) ) {
				throw new \Exception( 'пользователь существует' ) ;
			}

			if ( empty( $args[ 'photo' ] ) ) {
				throw new \Exception( 'фотография пуста' ) ;
			}

			/**
			* @var string $image_type - тип переданного изображения
			*/
			$image_type = strToLower( substr( $args[ 'photo' ] , 11 , strpos( $args[ 'photo' ] , ',' ) - 18 ) ) ;

			switch ( $image_type ) {
				case 'jpeg' :
				case 'jpg' : {
					$image_type = 'jpg' ;

					break ;
				}
				case 'png' :
				case 'gif' : {
					break ;
				}
				default : {
					throw new \Exception( 'неизвестный тип фотографии' ) ;
				}
			}

			/**
			* @var string $image_data - изображение в бинарном представлении
			*/
			$image_data = str_replace( ' ' , '+' , $args[ 'photo' ] ) ;
			$image_data =  substr( $image_data , strpos( $image_data , ',' ) + 1 ) ;
			$image_data = base64_decode( $image_data ) ;

			/**
			* @var string $photo - путь к файлу с изображением
			*/
			$photo = sprintf( 'photo/%s.%s' , sha1( $image_data ) , $image_type ) ;

			if ( ! file_put_contents( $photo , $image_data ) ) {
				throw new \Exception( 'не удалось записать файл' ) ;
			}

			/**
			* @var \PDOStatement $sth - подготовленный запрос
			*/
			$sth = $this->dbh->prepare( '
INSERT INTO
	`user`
SET
	`login` := :login ,
	`password` := sha1( :password ) ,
	`photo` := :photo ;
			' ) ;
			$sth->execute( array(
				'login' => $args[ 'login' ] ,
				'password' => $args[ 'password1' ] ,
				'photo' => $photo
			) ) ;
			$sth->closeCursor( ) ;

			return $this->login( array(
				'login' => $args[ 'login' ] ,
				'password' => $args[ 'password1' ]
			) ) ;
		}
	}