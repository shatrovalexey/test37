<?php
	/** класс для согласованного выполнения программ пакета
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/

	namespace Application ;

	/**
	* @subpackage Application\Controller контроллер
	*/
	class Controller extends Base {
		/**
		* @var Model\User $user - объект модели пользователя
		*/
		protected $user ;

		/**
		* Конструктор
		*/
		public function __construct( $app ) {
			parent::__construct( $app ) ;

			$this->user = new Model\User( $this ) ;
		}

		/**
		* Получение значений HTTP-аргументов.
		* @param array $name имена аргументов HTTP-запроса
		* @return mixed
		* если передано только одно имя аргумента, то возвращается скаляр,
		* если передано много имён, то возвращает массив
		*/
		protected function __arg( ) {
			$request = &$this->creator->request ;
			$result = array( ) ;

			foreach ( func_get_args( ) as $name ) {
				if ( ! isset( $request[ $name ] ) ) {
					$result[] = null ;

					continue ;
				}

				$result[] = $request[ $name ] ;
			}

			if ( count( $result ) == 1 ) {
				return $result[ 0 ] ;
			}

			return $result ;
		}

		/**
		* Оформление ответа для вывода в формате JSON
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		* result.data - данные для вывода в теле HTTP-ответа
		* result.passthru - выводить представление не внутри общего представления, а сразу
		* result.key - имя переменной для присвоения массива сообщений
		*/
		protected function __json( &$data ) {
			return array(
				'result' => array(
					'data' => $data
				) ,
				'view' => 'json' ,
				'headers' => array(
					array(
						$this->creator->config->http->header->default_name ,
						$this->creator->config->http->header->javascript
					)
				) ,
				'key' => 'message' ,
				'passthru' => true
			) ;
		}

		/**
		* Главная страница
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		*/
		public function indexAction( ) {
			return array(
				'view' => 'index' ,
				'result' => array( )
			) ;
		}

		/**
		* Выполнить метод
		* @param string $method_name - имя метода для выполнения.
		* @throws \Exception
		* @return array - результат выполнения метода $method_name
		*/
		public function call( $method_name ) {
			try {
				$result = $this->user->$method_name( $this->creator->request , $this->creator->files ) ;
			} catch ( \Exception $exception ) {
				$result = array(
					'error' => $exception->getMessage( )
				) ;
			}

			return $this->__json( $result ) ;
		}

		/**
		* Регистрация пользователя
		* @throws \Exception
		* @return array информация о пользователе
		* result - дополнительные данные для обработки запроса
		*/
		public function createAction( ) {
			return $this->call( 'create' ) ;
		}

		/**
		* Авторизация пользователя
		* @throws \Exception
		* @return array идентификатор сессии
		* result - дополнительные данные для обработки запроса
		*/
		public function loginAction( ) {
			return $this->call( 'login' ) ;
		}

		/**
		* Разавторизация пользователя
		* @return array результат
		* @throws \Exception
		* result - дополнительные данные для обработки запроса
		*/
		public function unloginAction( ) {
			return $this->call( 'unlogin' ) ;

		}

		/**
		* Профиль пользователя
		* @throws \Exception
		* @return array данные пользователя
		*/
		public function profileAction( ) {
			return $this->call( 'profile' ) ;
		}
	}