jQuery( function( ) {
	var $login_form = jQuery( "#login-form" ) ;
	var $create_form = jQuery( "#create-form" ) ;
	var $profile_form = jQuery( "#profile-form" ) ;
	var $all_forms = jQuery( [ $login_form , $create_form , $profile_form ] ) ;
	var $waiter = jQuery( ".waiter" ) ;

	var $reset_forms = function( ) {
		$all_forms.each( function( ) {
			jQuery( this ).get( 0 ).reset( ) ;
		} ) ;
	} ;

	var $lock_forms = function( $lock ) {
		$waiter[ $lock ? "removeClass" : "addClass" ]( "nod" ) ;
		$all_forms.find( ":input" ).prop( "disabled" , $lock == true ) ;

		return false ;
	} ;
	var $failure_form = function( $message ) {
		if ( $message == null ) {
			$message = "внутренняя ошибка" ;
		}

		this.find( ".error-field" ).text( $message ) ;

		return ;
	} ;
	var $process_form = function( ) {
		var $form = jQuery( this ) ;

		$form.find( ".error-field" ).empty( ) ;
		$lock_forms( true ) ;

		jQuery.ajax( {
			"url" : $form.attr( "action" ) ,
			"type" : $form.attr( "method" ) ,
			"data" : $form.serialize( ) ,
			"dataType" : "json" ,
			"context" : $form ,
			"success" : function( $data ) {
				var $form = jQuery( this ) ;

				$lock_forms( false ) ;
				$reset_forms( ) ;

				if ( ! ( "data" in $data ) ) {
					$failure_form.call( $form ) ;

					return ;
				}

				if ( "error" in $data.data ) {
					$failure_form.call( $form , $data.data.error ) ;

					return ;
				}

				jQuery( $form.data( "hide" ) ).addClass( "nod" ) ;
				jQuery( $form.data( "show" ) ).removeClass( "nod" ) ;

				$form.find( "[data-field]" ).each( function ( ) {
					var $node = jQuery( this ) ;
					var $field_name = $node.data( "field" ) ;
					var $field_type = $node.data( "type" ) ;
					var $field_value = "" ;

					if ( $field_name in $data.data ) {
						$field_value = $data.data[ $field_name ] ;
					}

					switch ( $field_type ) {
						case "text" : {
							$node.text( $field_value ) ;

							break ;
						}
						default : {
							$node.attr( $field_type , $field_value ) ;

							break ;
						}
					}
				} ) ;

				var $next = $form.data( "next" ) ;

				if ( $next ) {
					$form = jQuery( $next ) ;
					$form.find( "input[type=hidden]" ).each( function( ) {
						var $field = jQuery( this ) ;
						var $field_name = $field.attr( "name" ) ;
						var $field_value = $data.data[ $field_name ] ;

						$field.val( $field_value ) ;
					} ) ;
					$process_form.call( $form ) ;
				}
			} ,
			"failure" : $failure_form
		} ) ;

		return false ;
	} ;

	$reset_forms( ) ;
	jQuery( $login_form , $create_form ).submit( $process_form ) ;

	$create_form.find( "input[name=photo-input]" ).on( "change" , function( ) {
		var $file = jQuery( this ).get( 0 ).files[ 0 ] ;
		var $form = jQuery( this ).parents( "form:first" ) ;
		var $file_reader = new FileReader( ) ;

		$file_reader.onloadend = function( ) {
			$form.find( "input[name=photo]" ).val( $file_reader.result ) ;
			$lock_forms( false ) ;
		} ;
		$lock_forms( true ) ;
		$file_reader.readAsDataURL( $file ) ;
	} ) ;

	jQuery( ".profile-logout" ).on( "click" , function( ) {
		$lock_forms( true ) ;
		location.reload( ) ;

		return false ;
	} ) ;
} ) ;